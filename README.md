To-Do list app done in Python with Django Framework and deployed in heroku.
He has the function of create new users, delete e edit users, create categories of tasks and add task to which category you choose, add
and remove categories and tasks, see tasks done and undone tasks completed.

You can see the app on towards the following link:

[Todo List](https://fpp-todo.herokuapp.com/)

Home Page
![HomePage](https://i.imgur.com/PtbPEtj.png)

Categories
![Categories](https://i.imgur.com/0PEralC.png)

Tasks
![Tasks](https://i.imgur.com/v9GkarW.png)

Tasks Completed
![Tasks Completed](https://i.imgur.com/auxwrhq.png)

Change Password
![Change Pwd](https://i.imgur.com/fwuiq1M.png)

