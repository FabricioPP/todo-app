from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('list_category/', views.ListCategoryView.as_view(), name='list_category'),
    path('new/', views.CreateTodoView.as_view(), name='new_todo'),
    path('<int:category_pk>/new/', views.CreateTodoWithCategoryView.as_view(), name='new_todo_category'),
    path('<int:pk>/list/', views.ListTodoView.as_view(), name='list_todo'),
    path('<int:pk>/<int:cat_pk>/delete/', views.DeleteTodoView.as_view(), name='delete_todo'),
    path('<int:pk>/update/', views.UpdateView.as_view(), name='update_todo'),
    path('<int:pk>/detail/', views.DetailTodoView.as_view(), name='detail_todo'),
    path('<int:pk>/todo_done/', views.todo_done, name='todo_done'),
    path('<int:pk>/completed_todo', views.ListCompletedTodoView.as_view(), name='completed_todo'),

    #Categories urls
    path('category/new/', views.CreateCategoryView.as_view(), name='new_category'),
    path('category/<int:pk>/delete', views.DeleteCategoryView.as_view(), name='delete_category'),
]