from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse_lazy, reverse
from django.utils import timezone

# Create your models here.
class Todo(models.Model):
    title = models.CharField(max_length = 50, null=False)
    text = models.CharField(max_length = 150, null=False)
    author = models.ForeignKey(
        User,
        on_delete = models.CASCADE
        )
    category = models.ForeignKey(
        'Category',
        on_delete=models.CASCADE
    )
    created = models.DateField(default=timezone.now(), editable=True)
    completed = models.BooleanField(null=False, blank=False, default=False)

    completed_date = models.DateField(null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('list_todo', kwargs={'pk':self.category.id})

    def save(self, *args, **kwargs):
        if self.completed:
            self.completed_date = timezone.now()
        return super(Todo, self).save(*args, **kwargs)

class Category(models.Model):
    title = models.CharField(max_length=140)
    author = models.ForeignKey(
        User,
        on_delete = models.CASCADE,
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('list_category')

    @property
    def get_todos(self):
        return Todo.objects.filter(category__id=self.id)