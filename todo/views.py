from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, CreateView, DetailView
from django.views.generic.edit import UpdateView, DeleteView
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Todo, Category

# Create your views here.
class HomePageView(ListView):
    template_name = 'home.html'
    model = Category

# Category CRUD
class CreateCategoryView(LoginRequiredMixin, CreateView):
    template_name = 'category/new_category.html'
    model = Category
    fields = ['title',]
    success_url = reverse_lazy('list_category')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(CreateCategoryView, self).form_valid(form)

class DeleteCategoryView(LoginRequiredMixin, DeleteView):
    model = Category
    template_name = 'category/delete_category.html'
    success_url = reverse_lazy('list_category')

class ListCategoryView(LoginRequiredMixin, ListView):
    model = Category
    template_name = 'category_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['Cat_count'] = Category.objects.all().count()
        context['Todo_count'] = Todo.objects.all().count()
        return context




# TODOs Completed View
class ListCompletedTodoView(LoginRequiredMixin, ListView):
    template_name = 'completed_todo.html'
    model = Todo

    def get_queryset(self):
        self.category = get_object_or_404(Category, id=self.kwargs['pk'])
        return Todo.objects.filter(category__id= self.category.id)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['category'] = self.category
        return context

#Function to change the status of the task
def todo_done(request, pk:int):

    # Changes make trough forms(list_todo button)
    if request.method == 'POST':
        todo = get_object_or_404(Todo, pk=pk)
        cat_id = todo.category.id
        if todo.completed:
            # Undone task completed
            todo.completed = False
            todo.save()

            redir_url = reverse('completed_todo', kwargs={
                'pk': cat_id
            })

            return redirect(redir_url)
        else:
            # Done tasks incompleted
            todo.completed = True
            todo.save()

            redir_url = reverse('list_todo', kwargs={
                'pk': cat_id
            })

            return redirect(redir_url)
    # Changes make trough simple buttons like todo detail
    else:
        todo = get_object_or_404(Todo, pk=pk)
        cat_id = todo.category.id
        if todo.completed:
            # Undone task completed
            todo.completed = False
            todo.save()

            return redirect(reverse('detail_todo', kwargs={
                'pk':todo.pk,
            }))
        else:
            # Done tasks incompleted
            todo.completed = True
            todo.save()


            return redirect(reverse('detail_todo', kwargs={
                'pk':todo.pk,
            }))
    return render(request, reverse('home'))


# TO-DO CRUD
class ListTodoView(LoginRequiredMixin, ListView):
    template_name = 'todo_list.html'

    def get_queryset(self):
        self.category = get_object_or_404(Category, id=self.kwargs['pk'])
        return Todo.objects.filter(category__id= self.category.id)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['category'] = self.category
        return context

class DetailTodoView(LoginRequiredMixin, DetailView):
    model = Todo
    template_name = 'detail_todo.html'

class CreateTodoView(LoginRequiredMixin, CreateView):
    template_name = 'new_todo.html'
    model = Todo
    fields = ['title', 'text', 'category']

    success_url = reverse_lazy('list_category')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(CreateTodoView, self).form_valid(form)

class CreateTodoWithCategoryView(LoginRequiredMixin, CreateView):
    template_name = 'new_todo.html'
    model = Todo
    fields = ['title', 'text',]
    

    def dispatch(self, request, *args, **kwargs):
        """
        Overridden so we can make sure the `Ipsum` instance exists
        before going any further.
        """
        self.category = get_object_or_404(Category, pk=kwargs['category_pk'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.category = self.category
        return super(CreateTodoWithCategoryView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.category.pk
        return context

class DeleteTodoView(LoginRequiredMixin, DeleteView):
    template_name = 'delete_todo.html'
    model = Todo
    success_url = 'home'

    def delete(self, request, *args, **kwargs):
        self.category = get_object_or_404(Category, pk=kwargs['cat_pk'])
        self.object = self.get_object()
        self.object.delete()
        success_url = self.get_success_url()
        success_url = reverse('list_todo', kwargs={'pk': self.category.pk,})

        return redirect(success_url)



class UpdateView(LoginRequiredMixin, UpdateView):
    model = Todo
    template_name = 'update_todo.html'
    fields = ['title', 'text',]